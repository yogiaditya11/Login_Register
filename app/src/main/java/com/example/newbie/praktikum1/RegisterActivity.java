package com.example.newbie.praktikum1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    private EditText edtNama, edtAlmt, edtNo, edtEmail;
    private CheckBox chkBola, chkMusik, chkvoly, chkfutsal;
    private Button btn1;
    private RadioGroup r1;
    public String hobi1, hobi2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btn1 = findViewById(R.id.btn_proses_regis);
        chkBola = findViewById(R.id.chk_bola);
        chkMusik = findViewById(R.id.chk_musik);
        chkvoly = findViewById(R.id.chk_volly);
        chkfutsal = findViewById(R.id.chk_futsal);
        edtNama = findViewById(R.id.txt_input_nama);
        edtAlmt = findViewById(R.id.txt_input_almt);
        edtEmail = findViewById(R.id.txt_input_email);
        edtNo = findViewById(R.id.txt_input_no);
        r1 = findViewById(R.id.rdg_01);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, HasilRegisActivity.class);
                intent.putExtra("nama", edtNama.getText().toString());
                intent.putExtra("alamat", edtAlmt.getText().toString());
                intent.putExtra("no", edtNo.getText().toString());
                intent.putExtra("email", edtEmail.getText().toString());

                int id = r1.getCheckedRadioButtonId();
                RadioButton radioButton = findViewById(id);
                intent.putExtra("jk", radioButton.getText().toString());

                String xx = "";
                if(chkBola.isChecked()) {
                    xx+= " " + chkBola.getText().toString();
                }

                if(chkMusik.isChecked()) {
                    xx+= " " + chkMusik.getText().toString();
                }

                if(chkvoly.isChecked()) {
                    xx+= " " + chkvoly.getText().toString();
                }

                if(chkfutsal.isChecked()) {
                    xx+= " " + chkfutsal.getText().toString();
                }

                String coba = xx.replaceFirst(" ", "");
                intent.putExtra("hobi", coba);

                startActivityForResult(intent, 0);
            }
        });


    }
}
