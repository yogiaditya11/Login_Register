package com.example.newbie.praktikum1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText username = findViewById(R.id.txt_user);
        final EditText password = findViewById(R.id.txt_pass);

        Button btnlogin =  findViewById(R.id.btn_proses_login);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = username.getText().toString();
                String pwd = password.getText().toString();
                if(nama.equals("admin")&& pwd.equals("admin")) {
                    Toast.makeText(getApplicationContext(),"Username & Password Cocok", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this, SuksesLoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(),"Username & Password salah", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
